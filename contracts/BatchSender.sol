// SPDX-License-Identifier: MIT
// UBDNDistributor ERC20 Token Distributor
pragma solidity 0.8.19;


import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";

/// @title BatchSender
/// @author Envelop Team
/// @notice Use send batch erc20
/// @dev  need approve before call
contract BatchSender is Ownable {
    using SafeERC20 for IERC20;
    
    IERC20 public erc20;

    constructor(address _erc20){
        erc20 = IERC20(_erc20);
    }



    function multiTransfer(address[] memory _receivers, uint256[]  memory _values) public {
        require(_receivers.length == _values.length, "Not equal arrays");

        for (uint i=0; i< uint8(_receivers.length); i++){
            erc20.safeTransferFrom(msg.sender, _receivers[i], _values[i]);
        }
    }

    

    ///////////////////////////////////////////////////////////
    ///////    Admin Functions        /////////////////////////
    ///////////////////////////////////////////////////////////
    function seterc20(address _token) 
        external 
        onlyOwner 
    {
        erc20 = IERC20(_token);
    }

    ///////////////////////////////////////////////////////////

    
}