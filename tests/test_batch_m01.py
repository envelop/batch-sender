import pytest
import logging
from brownie import Wei, reverts, chain
LOGGER = logging.getLogger(__name__)

PAY_AMOUNT = 100_000e6
def test_distrib_simple(accounts, batchsender, usdt, usdc):
    
    RECEIVERS = [accounts[1], accounts[2],accounts[3],accounts[4],accounts[5]]
    AMOUNTS = [10, 20, 30, 40, 50]
    usdt.approve(batchsender, sum(AMOUNTS), {'from':accounts[0]})
    tx = batchsender.multiTransfer(RECEIVERS, AMOUNTS, {'from':accounts[0]})

    for e in tx.events.keys():
        logging.info('Events:{}:{}'.format(e, tx.events[e]))
    assert len(tx.events['Transfer']) == len(RECEIVERS)

    for i in range(len(AMOUNTS)):
        assert usdt.balanceOf(RECEIVERS[i]) == AMOUNTS[i]
    


